package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"registerGateway/routes"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"github.com/joho/godotenv"
)

func main() {
	//Environment
	errLoadEnv := godotenv.Load("local.env")
	if errLoadEnv != nil {
		log.Fatalf("Err load env : ", errLoadEnv)
	}

	port := os.Getenv("PORT")
	if port != "" {
		fmt.Println("ENV PORT : ", port)
	} else {
		fmt.Println("ENV PORT : ", port)
	}

	//Disable
	//gin.DisableConsoleColor()

	// Enable log's color
	gin.ForceConsoleColor()

	// //Set write loging file
	// f, _ := os.Create("gin.log")
	// gin.DefaultWriter = io.MultiWriter(f)

	r := gin.Default()

	//Size upload file
	r.MaxMultipartMemory = 8 << 20 // 8 MiB

	r.Use(gin.LoggerWithFormatter(func(param gin.LogFormatterParams) string {
		// your custom format
		return fmt.Sprintf("%s - [%s] \"%s %s %s %d %s \"%s\" %s\"\n",
			param.ClientIP,
			param.TimeStamp.Format(time.RFC1123),
			param.Method,
			param.Path,
			param.Request.Proto,
			param.StatusCode,
			param.Latency,
			param.Request.UserAgent(),
			param.ErrorMessage,
		)
	}))

	//Use middleware
	r.Use(getMiddleware())

	//r.Use(gin.Recovery())

	s := &http.Server{
		Addr:    ":" + port,
		Handler: r,
	}

	//Router
	routes.ServeRoutes(r)

	//r.Run()
	s.ListenAndServe()
}

func getMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		uuid := uuid.New()
		c.Set("uuid", uuid)
		fmt.Printf("The request uuid with %s is started \n", &uuid)
		c.Next()
		fmt.Printf("The request uuid with %s is serverd \n", &uuid)
	}
}
