package controllers

import (
	"fmt"
	"net/http"
	"reflect"
	"registerGateway/config"
	"registerGateway/model"
	"registerGateway/validation"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

var userByEmail struct {
	Email string `json:"email" binding:"required"`
}

type Response struct{}

//var validate = validator.New()

func (res Response) GetUserByEmail(body *gin.Context) {
	if err := body.ShouldBind(&userByEmail); err != nil {
		body.JSON(http.StatusBadRequest, gin.H{
			"status":  400,
			"success": false,
			"data":    "Invalid data",
		})
		return
	}

	fmt.Println("Email : ", userByEmail.Email)

	db, ctx, client := config.ConnectDB()

	// เรียกดู collection
	collection := db.Collection("users")

	filter := bson.M{"email": userByEmail.Email}

	var user model.Users

	errFindOne := collection.FindOne(ctx, filter).Decode(&user)
	defer client.Disconnect(ctx)

	if errFindOne != nil {
		fmt.Println("Err findOne :", errFindOne)
		body.JSON(http.StatusOK, gin.H{
			"status":  200,
			"success": false,
			"data":    "Not found data",
		})
		return
	}

	fmt.Println("result: ", user.Email)

	body.JSON(http.StatusOK, gin.H{
		"status":  200,
		"success": true,
		"data":    user,
	})
	return
}

func (res Response) GetUserFromID(ctx *gin.Context) {
	userId := ctx.Param("_id")
	fmt.Println("userId : ", userId)

	objId, errHex := primitive.ObjectIDFromHex(userId)
	if errHex != nil {
		fmt.Println("Hex err: ", errHex)
		ctx.JSON(http.StatusOK, gin.H{
			"status":  400,
			"success": false,
			"data":    "Bad request",
		})
		return
	}

	fmt.Println("User objId: ", objId)

	db, context, client := config.ConnectDB()

	filter := bson.M{"_id": objId}
	var user model.Users
	data := db.Collection("users").FindOne(context, filter).Decode(&user)
	defer client.Disconnect(context)

	if data != nil {
		fmt.Println("Find user by id err : ", data)
		ctx.JSON(http.StatusOK, gin.H{
			"status":  200,
			"success": false,
			"data":    "Not found user",
		})
		return
	}

	ctx.JSON(http.StatusOK, gin.H{
		"status":  200,
		"success": true,
		"data":    "Get user",
		"obj":     user,
	})
	return
}

var validate *validator.Validate

func (r Response) Register(ctx *gin.Context) {

	validate := validator.New()

	var user validation.Register
	if err := ctx.ShouldBindJSON(&user); err != nil {
		fmt.Println("Validate register : ", err)
		ctx.JSON(http.StatusOK, gin.H{
			"status":  400,
			"success": false,
			"data":    err.Error(),
		})
		return
	}

	errValidate := validate.Struct(user)
	if errValidate != nil {
		fmt.Println("Validate register: ", errValidate)
		ctx.JSON(http.StatusOK, gin.H{
			"status":  400,
			"success": false,
			"data":    errValidate.Error(),
		})
		return
	}

	db, context, client := config.ConnectDB()

	//Find email duplicate
	var userModel model.Users
	errDup := db.Collection("users").FindOne(context, bson.M{"email": user.Email}).Decode(&userModel)
	if errDup == nil {
		fmt.Println("Duplicate : ", errDup)
		fmt.Println("Dupcate email: ", userModel.Email)
		ctx.JSON(http.StatusOK, gin.H{
			"status":  200,
			"success": false,
			"data":    "Email is duplicate",
		})
		return
	}

	//Create user
	create, errCreate := db.Collection("users").InsertOne(context, bson.M{
		"email":       user.Email,
		"firstName":   user.FirstName,
		"lastName":    user.LastName,
		"phoneNumber": user.PhoneNumber,
	})

	fmt.Println("create: ", create)
	fmt.Println("errCreate: ", errCreate)

	defer client.Disconnect(context)

	ctx.JSON(http.StatusOK, gin.H{
		"status":  200,
		"success": true,
		"data":    "Register success",
	})

	return
}

func (r Response) UpdateUser(ctx *gin.Context) {
	userId := ctx.Param("_id")
	//fmt.Println("UserId: ", userId)

	objId, errHex := primitive.ObjectIDFromHex(userId)
	if errHex != nil {
		fmt.Println("ErrHex : ", errHex)
		ctx.JSON(http.StatusOK, gin.H{
			"status":  200,
			"success": false,
			"data":    errHex.Error(),
		})
		return
	}

	fmt.Println("User objId: ", objId)

	validate := validator.New()
	var userModel validation.UpdateUser
	if err := ctx.ShouldBindJSON(&userModel); err != nil {
		fmt.Println("Validate register : ", err)
		ctx.JSON(http.StatusOK, gin.H{
			"status":  400,
			"success": false,
			"data":    err.Error(),
		})
		return
	}

	errValidate := validate.Struct(userModel)
	if errValidate != nil {
		fmt.Println("errValidate: ", errValidate)
		ctx.JSON(http.StatusBadRequest, gin.H{
			"status":  400,
			"success": false,
			"data":    errValidate.Error(),
		})
		return
	}

	db, context, client := config.ConnectDB()

	//Find user by id
	var user model.Users
	errFind := db.Collection("users").FindOne(context, bson.M{"_id": objId}).Decode(&user)
	if errFind != nil {
		fmt.Println("Err find: ", errFind)
		ctx.JSON(http.StatusOK, gin.H{
			"status":  200,
			"success": false,
			"data":    errFind.Error(),
		})
		return
	}

	fmt.Println("Find user: ", user)

	//Update User
	filter := bson.M{"_id": objId}
	queryUpdate := bson.D{{Key: "$set", Value: bson.M{
		"email":       userModel.Email,
		"firstName":   userModel.FirstName,
		"lastName":    userModel.LastName,
		"phoneNumber": userModel.PhoneNumber,
	}}}
	update, errUpdate := db.Collection("users").UpdateOne(context, filter, queryUpdate)

	if errUpdate != nil {
		fmt.Println("errUpdate: ", errUpdate)
	}
	defer client.Disconnect(context)

	fmt.Println("Update: ", update.MatchedCount, update.ModifiedCount)

	ctx.JSON(http.StatusOK, gin.H{
		"status":  200,
		"success": true,
		"data":    "Update user success",
	})
	return
}

func (r Response) PushDataSubject(ctx *gin.Context) {
	userId := ctx.Param("_id")

	objId, errHex := primitive.ObjectIDFromHex(userId)
	if errHex != nil {
		fmt.Println("Err hex: ", errHex.Error())
		ctx.JSON(http.StatusOK, gin.H{
			"status":  200,
			"success": true,
			"data":    errHex.Error(),
		})
		return
	}

	validate := validator.New()
	var dataSubjectModel validation.PushDataSubject
	if errBind := ctx.ShouldBindJSON(&dataSubjectModel); errBind != nil {
		fmt.Println("Bind struct : ", errBind.Error())
		ctx.JSON(http.StatusOK, gin.H{
			"status":  200,
			"success": false,
			"data":    errBind.Error(),
		})
		return
	}

	errValidate := validate.Struct(dataSubjectModel)
	if errValidate != nil {
		fmt.Println("errValidate: ", errValidate.Error())
		ctx.JSON(http.StatusOK, gin.H{
			"status":  200,
			"success": false,
			"data":    errValidate.Error(),
		})
		return
	}

	if len(dataSubjectModel.DataSubject) == 0 {
		fmt.Println("DataSubject is len 0")
		ctx.JSON(http.StatusOK, gin.H{
			"status":  200,
			"success": false,
			"data":    "DataSubject len is 0",
		})
		return
	}

	var user model.Users
	db, context, client := config.ConnectDB()

	//Find user
	filterFindUser := bson.M{"_id": objId}
	errFind := db.Collection("users").FindOne(context, filterFindUser).Decode(&user)

	defer client.Disconnect(context)

	if errFind != nil {
		fmt.Println("Find user: ", errFind.Error())

		ctx.JSON(http.StatusOK, gin.H{
			"status":  200,
			"success": false,
			"data":    "Not found user ",
		})
		return
	}

	//Push data
	var filterUpdate = bson.M{"_id": objId}
	for i := 0; i < len(dataSubjectModel.DataSubject); i++ {
		subject := dataSubjectModel.DataSubject[i]
		var queryPush = bson.D{{Key: "$push", Value: bson.M{
			"dataSubject": bson.M{
				"subjectId":   subject.SubjectID,
				"subjectName": subject.SubjectName,
			},
		}}}

		pushData, errPushData := db.Collection("users").UpdateOne(context, filterUpdate, queryPush)
		if errPushData != nil {
			fmt.Println("Err push data: ", errPushData)
		} else {
			fmt.Println("Push data: ", pushData)
		}
	}

	ctx.JSON(http.StatusOK, gin.H{
		"status":  200,
		"success": true,
		"data":    "Push data success",
	})
	return
}

func (r Response) PullSubjectData(ctx *gin.Context) {
	userId := ctx.Param("_id")

	objId, errHex := primitive.ObjectIDFromHex(userId)
	if errHex != nil {
		fmt.Println("Err hex : ", errHex)
		ctx.JSON(http.StatusOK, gin.H{
			"status":  200,
			"success": false,
			"data":    errHex.Error(),
		})
		return
	}

	validate = validator.New()
	var dataSubjectModel validation.PullDataSubject
	if errBind := ctx.ShouldBindJSON(&dataSubjectModel); errBind != nil {
		fmt.Println("Bind struct : ", errBind.Error())
		ctx.JSON(http.StatusOK, gin.H{
			"status":  200,
			"success": false,
			"data":    errBind.Error(),
		})
		return
	}

	fmt.Println("subjectId: %s type %s", dataSubjectModel.SubjectId, reflect.TypeOf(dataSubjectModel.SubjectId))
	var user model.Users
	db, context, client := config.ConnectDB()
	defer client.Disconnect(context)

	//Find user
	fmt.Println("objId: ", objId)
	filterFindUser := bson.M{"_id": objId}
	errFindUser := db.Collection("users").FindOne(context, filterFindUser).Decode(&user)
	if errFindUser != nil {
		fmt.Println("Find user err: ", errFindUser.Error())
		ctx.JSON(http.StatusOK, gin.H{
			"status":  200,
			"success": false,
			"data":    errFindUser.Error(),
		})
		return
	}

	fmt.Println("Find user: ", user.Email)

	//Pull data
	var filterPullData = bson.M{"$and": []bson.M{
		bson.M{"_id": objId},
		bson.M{"dataSubject": bson.M{"$elemMatch": bson.M{"subjectId": dataSubjectModel.SubjectId}}},
	}}
	//var queryPull = bson.M{"$pull": bson.M{"dataSubject": bson.M{"$elemMatch": bson.M{"subjectId": dataSubjectModel.SubjectId}}}}
	var queryPull = bson.D{{Key: "$pull", Value: bson.M{
		"dataSubject": bson.M{
			"subjectId": dataSubjectModel.SubjectId,
		},
	}}}

	pullData, errPull := db.Collection("users").UpdateOne(context, filterPullData, queryPull)

	if errPull != nil {
		fmt.Println("Pull data err: ", errPull.Error())
		ctx.JSON(http.StatusOK, gin.H{
			"status":  200,
			"success": false,
			"data":    errPull.Error(),
		})
	} else {
		fmt.Println("Pull data success: ", pullData)
		ctx.JSON(http.StatusOK, gin.H{
			"status":  200,
			"success": true,
			"data":    "Pull data success",
		})
		return
	}
}
