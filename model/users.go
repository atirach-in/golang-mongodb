package model

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Users struct {
	ID           primitive.ObjectID `bson:"_id"`
	Subject      []string           `bson:"subject"`
	DataSubject  []*DataSub         `bson:"dataSubject"`
	Id           string             `bson:"id"`
	FirstName    string             `bson:"firstName"`
	LastName     string             `bson:"lastName"`
	Email        string             `bson:"email"`
	AccessToken  string             `bson:"accessToken"`
	RefreshToken string             `bson:"refreshToken"`
	CreatedAt    time.Time          `bson:"createdAt"`
	UpdatedAt    time.Time          `bson:"updatedAt"`
	PhoneNumber  string             `bson:"phoneNumber"`
}

type DataSub struct {
	SubjectID   string `json:"subjectId" binding:"required" validate:"min=1,max=200"`
	SubjectName string `json:"subjectName" binding:"required" validate:"min=1,max=200"`
}
