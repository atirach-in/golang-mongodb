package config

import (
	"context"
	"log"
	"os"
	"time"

	"github.com/joho/godotenv"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

func ConnectDB() (*mongo.Database, context.Context, *mongo.Client) {

	errLoadEnv := godotenv.Load("local.env")
	if errLoadEnv != nil {
		log.Fatal(errLoadEnv)
	}

	port := os.Getenv("PORT_DB")
	host := os.Getenv("HOST_DB")
	dbName := os.Getenv("DATABASE_NAME")

	connectionURI := "mongodb://" + host + ":" + port
	client, err := mongo.NewClient(options.Client().ApplyURI(connectionURI))
	if err != nil {
		log.Fatal(err)
	}

	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)

	err = client.Connect(ctx)
	if err != nil {
		log.Fatal(err)
	}

	err = client.Ping(ctx, readpref.Primary())
	if err != nil {
		log.Fatal(err)
	}

	// เรียกดู database
	db := client.Database(dbName)

	return db, ctx, client

}
