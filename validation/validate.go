package validation

type Register struct {
	Email       string `json:"email" binding:"required" validate:"email"`
	FirstName   string `json:"firstname" binding:"required" validate:"min=1,max=200"`
	LastName    string `json:"lastname" binding:"required" validate:"min=1,max=200"`
	PhoneNumber string `json:"phoneNumber" binding:"required" validate:"min=1,max=10"`
}

type UpdateUser struct {
	Email       string `json:"email" binding:"required" validate:"email"`
	FirstName   string `json:"firstname" binding:"required" validate:"min=1,max=200"`
	LastName    string `json:"lastname" binding:"required" validate:"min=1,max=200"`
	PhoneNumber string `json:"phoneNumber" binding:"required" validate:"min=1,max=10"`
}

type DataSub struct {
	SubjectID   string `json:"subjectId" binding:"required" validate:"min=1,max=200"`
	SubjectName string `json:"subjectName" binding:"required" validate:"min=1,max=200"`
}

type PushDataSubject struct {
	DataSubject []*DataSub `json:"dataSubject" binding:"required" validate:"dive"`
}

type PullDataSubject struct {
	SubjectId string `json:"subjectId" binding:"required"`
}
