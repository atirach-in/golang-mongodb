package routes

import (
	"fmt"
	"log"
	"net/http"
	"registerGateway/controllers"
	"time"

	"github.com/gin-gonic/gin"
)

type StructA struct {
	FieldA string `form:"field_a"`
}

type StructB struct {
	NestedStruct StructA
	FieldB       string `form:"field_b"`
}

type StructC struct {
	NestedStructPointer *StructA
	FieldC              string `form:"field_c"`
}

type StructD struct {
	NestedAnnonyStruct struct {
		FieldX string `form:"field_x"`
	}
	FieldD string `form:"field_d"`
}

type Person struct {
	Name     string    `form:"name"`
	Address  string    `form:"address"`
	Birthday time.Time `form:"birthday" time_format:"2006-01-02" time_utc:"1"`
}

type PersonID struct {
	ID   string `uri:"id" binding:"required,uuid"`
	Name string `uri:"name" binding:"required"`
}

func GetDataB(c *gin.Context) {
	var b StructB
	c.Bind(&b)
	c.JSON(200, gin.H{
		"a": b.NestedStruct,
		"b": b.FieldB,
	})
}

func GetDataC(c *gin.Context) {
	var cc StructC
	c.Bind(&cc)
	c.JSON(200, gin.H{
		"a": cc.NestedStructPointer,
		"c": cc.FieldC,
	})
}

func GetDataD(c *gin.Context) {
	var b StructD
	c.Bind(&b)
	c.JSON(200, gin.H{
		"x": b.NestedAnnonyStruct,
		"d": b.FieldD,
	})
}

func startPage(c *gin.Context) {
	var person Person
	if c.ShouldBind(&person) == nil {
		log.Println(person.Name)
		log.Println(person.Address)
		log.Println(person.Birthday)
	}

	c.String(200, "Success")
}

var secrets = gin.H{
	"foo":    gin.H{"email": "foo@bar.com", "phone": "123433"},
	"austin": gin.H{"email": "austin@example.com", "phone": "666"},
	"lena":   gin.H{"email": "lena@guapa.com", "phone": "523443"},
}

func ServeRoutes(r *gin.Engine) {
	controller := controllers.Response{}

	r.GET("/health/check", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message": "Server is runnnig",
		})
	})

	//ASCII Response
	r.GET("/someJSON", func(c *gin.Context) {

		data := map[string]interface{}{
			"lang": "GOLANG",
			"tag":  "<h1>",
		}

		c.AsciiJSON(http.StatusOK, data)
	})

	v1 := r.Group("/v1")
	{
		v1.GET("/getb", GetDataB)
		v1.GET("/getc", GetDataC)
		v1.GET("/getd", GetDataD)
		v1.GET("/testing", startPage)
		v1.GET("/:name/:id", func(c *gin.Context) {
			var person PersonID
			if err := c.ShouldBindUri(&person); err != nil {
				log.Println("Err ", err)
				c.JSON(400, gin.H{
					"msg": err,
				})
				return
			}

			c.JSON(200, gin.H{
				"id":   person.ID,
				"name": person.Name,
			})
		})

		v1.POST("/post", func(c *gin.Context) {
			ids := c.QueryMap("ids")
			names := c.PostFormMap("names")

			fmt.Printf("ids: %v; names: %v", ids, names)
		})

		v1.GET("/redirect", func(c *gin.Context) {
			c.Redirect(http.StatusMovedPermanently, "http://www.google.com/")
		})

		v1.GET("/someJSON/Secure", func(ctx *gin.Context) {
			names := []string{"luna", "bob", "sam"}
			ctx.SecureJSON(http.StatusOK, names)
		})

		//Read data from response
		v1.GET("/someDataFromReader", func(ctx *gin.Context) {
			response, err := http.Get("https://raw.githubusercontent.com/gin-gonic/logo/master/color.png")

			if err != nil || response.StatusCode != http.StatusOK {
				ctx.Status(http.StatusServiceUnavailable)
				return
			}

			reader := response.Body
			contentLength := response.ContentLength
			contentType := response.Header.Get("Content-Type")

			extraHeaders := map[string]string{
				"Content-Disposition": `attachment; filename="gopher_2.png"`,
			}

			ctx.DataFromReader(http.StatusOK, contentLength, contentType, reader, extraHeaders)
		})

		//Serving static files
		v1.Static("assets", "./assets")
		v1.StaticFS("more_static", http.Dir("assets"))
		v1.StaticFile("/gopher.png", "./assets/gopher.png")

		//Set cookie
		v1.GET("/cookie", func(ctx *gin.Context) {
			cookie, err := ctx.Cookie("gin_cookie")
			if err != nil {
				cookie = "NotSet"
				ctx.SetCookie("gin_cookie", "test", 3600, "/", "localhost", false, true)
			}

			fmt.Printf("Cookie value: %s \n", cookie)
		})

		//Upload file
		v1.POST("/upload", func(ctx *gin.Context) {
			file, _ := ctx.FormFile("file")
			log.Println(file.Filename)

			err := ctx.SaveUploadedFile(file, "./assets/"+file.Filename)
			fmt.Printf("Upload file: %s \n", err)

			ctx.String(http.StatusOK, fmt.Sprintf("%s uploaded! ", file.Filename))

		})

		v1.POST("/user", controller.GetUserByEmail)
		v1.GET("/user/:_id", controller.GetUserFromID)
		v1.POST("/register", controller.Register)
		v1.PUT("/user/:_id", controller.UpdateUser)
		v1.PUT("/user/subject/data/:_id", controller.PushDataSubject)
		v1.PUT("/user/pull/subject/data/:_id", controller.PullSubjectData)
	}

	authorized := r.Group("/admin", gin.BasicAuth(gin.Accounts{
		"foo":    "bar",
		"austin": "1234",
		"lena":   "hello2",
		"manu":   "4321",
	}))

	authorized.GET("/secrets", func(c *gin.Context) {
		// get user, it was set by the BasicAuth middleware
		user := c.MustGet(gin.AuthUserKey).(string)
		if secret, ok := secrets[user]; ok {
			c.JSON(http.StatusOK, gin.H{"user": user, "secret": secret})
		} else {
			c.JSON(http.StatusOK, gin.H{"user": user, "secret": "NO SECRET :("})
		}
	})
}
